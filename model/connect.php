<?php
//classe qui instancie la connection a la base de donnée
class PdoNesti{

	private static $serveur= "mysql:host=localhost"; //permet la connection a la BDD
	private static $bdd="dbname=nesti";
	private static $user="admin1";
	private static $mdp="admin";
	private static $monPdo;
	private static $monPdoNesti=null;

private function __construct() //constructeur privé, crée l'instance de pdo qui sera sollicité
{
	PdoNesti::$monPdo = new PDO(PdoNesti::$serveur.';'.PdoNesti::$bdd,PdoNesti::$user,PdoNesti::$mdp);
	PdoNesti::$monPdo->query("SET CHARACTER SET utf8");

}

public  static function getPdoNesti()  //récupère la connexion 
{
		if(PdoNesti::$monPdoNesti == null) //si la connexion n'a pas eu lieu
		{
			PdoNesti::$monPdoNesti= new PdoNesti();
		}
		return PdoNesti::$monPdoNesti;  
	}



	public function __destruct(){ // détruit la connexion
		PdoNesti::$monPdo=null;
	}


// retourne un tableau contenant l'ingredient passé en paramètre
	public function getLesIngredients($idIng)
	{
		$req="select * from Ingredients where idIng = '$idIng'";
		$res = PdoNesti::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes; 
	}

	//récupère toutes les categories
	public function getLesCategories()
	{
		$req = "select * from categorie";
		$res = PdoNesti::$monPdo->query($req);
		$lesLignes = $res->fetchAll();
		return $lesLignes; 
	}


//fonction qui entre un nouveau utilisateur dans la bdd

	public function insertUser($name,$prenom,$ddn,$mail,$adresse,$login,$mdp){
		$req= "insert into Utilisateur(nomUser,prenom,ddnUser,adresse,mail,login,mdp) Values($name,$prenom,$ddn,$adresse,$mail,$login,$mdp)";
		$res= PdoNesti::$monPdo->query($req);

	}
//fonction qui selectionne les categories dans la table ingredient
	public function getLesIngredientsDeCategorie($idCat){
		$req= "select * from Ingredients where idCategorie =$idCat";
		$res= PdoNesti::$monPdo->query($req);
		$lesLignes= $res->fetchAll();
		return $lesLignes;
	}

	// récupère les tarifs
	public function getTarif($idIng){ 
		$req= "select C.nom,T.tarif,T.idCond,T.devise FROM Ingredients as I JOIN Tarif as T ON I.idIng = T.idIng JOIN Conditionnement as C ON C.id = T.idCond WHERE I.idIng=$idIng";
		$res = PdoNesti::$monPdo->query($req);
		$id=$res->fetchAll();
		return $id;
	}
	//  récupère le tarif en fonction du conditionnement et de l'id de l'ingredient
	public function getTarifCond($idIng,$idCond){
		$req="select C.nom ,T.tarif,T.devise FROM Ingredients as I JOIN Tarif as T ON I.idIng = T.idIng JOIN Conditionnement as C ON C.id = T.idCond WHERE I.idIng=$idIng and c.id=$idCond";
		$res=PdoNesti::$monPdo->query($req);
		$laLigne=$res->fetch();
		return $laLigne;

	}
	

	public function getLesProduitsDuTableau($desIdProduit)
	{
		$nbProduits = count($desIdProduit);
		$lesProduits=array();
		if($nbProduits != 0)
		{
			foreach($desIdProduit as $unIng  )
			{ 
				foreach ($unIng as $key) { 
					
					$unCond=$key['idCond'];
					$unIngredient=$key['idIng']; 
					$req = "select C.nom,I.nom,T.tarif,devise 
					from Ingredients as I 
					JOIN Tarif as T 
					ON T.idIng = I.idIng 
					JOIN Conditionnement as C 
					ON T.idCond=C.id 
					WHERE I.idIng =$unIngredient 
					AND C.id=$unCond";
					$res = PdoNesti::$monPdo->query($req);
					$unProduit = $res->fetch();
					$lesProduits[] = $unProduit;
				}
				
			}
		}
		return $lesProduits;
	}
	// checke si le login est correct
	public function checkLogin($login,$mdp){
		$req= "select id from Utilisateur where login=:m and mdp=:p";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":m",$login);
		$res->bindValue(":p",$mdp); 
		$res->execute();
		$id = $res->fetchColumn();
		return $id; 


	}
	// recupère l'utilisateur 
	public function getUser($id){
		$req="select login from Utilisateur where id=:i";
		$res=PdoNesti::$monPdo->prepare($req);
		$res->bindValue(":i",$id);
		$res->execute();
		$id = $res->fetchColumn();
		return $id;
	}

	//récupère les informations concernant l'utilisateur
	public function getInfoUser($id){
		$req="select * from Utilisateur where id=:i";
		$res=PdoNesti::$monPdo->prepare($req);
		$res->bindValue(":i",$id);
		$res->execute();
		$id = $res->fetchAll();

		return $id;
	}

	//crée un nouveau compte client
	public function insertClient($nom,$prenom,$login,$mail,$mdp){
		$succes = false;
		$req= "insert into Utilisateur(nom,prenom,mail,login,mdp)
		VALUES(:n,:p,:m,:l,:mdp)";
		$res=PdoNesti::$monPdo->prepare($req);
		$res->bindValue(":n",$nom);
		$res->bindValue(":p",$prenom);
		$res->bindValue(":l",$login);
		$res->bindValue(":mdp",$mdp);
		$res->bindValue(":m",$mail);
		$res->execute();
		
		$lesInsertion = $res->fetchColumn();
		
		if($lesInsertion == false){
			
			$succes = true;
		}
		else{
			$succes = false;
		}
		return $succes;

	}


	public function modifProfil($id ,$nom,$prenom){
		$req = "update Utilisateur SET nom = :n, prenom = :p WHERE Utilisateur.id = :i";
		$res->bindValue(":n",$nom);
		$res->bindValue(":p",$prenom);
		$res->bindValue(":i",$id);
		$res->execute();
		

	}

	//fonctions pour l'ajout d'adresse
	public function ajoutAdresse($rue,$cp,$ville){
		
		$getMax = "select max(id)  from adresse";
		$res = PdoNesti::$monPdo->query($getMax);
		$id = $res->fetchColumn();
		$id++;

		$req="insert into adresse VALUES($id,'$rue',$cp,'$ville')";

		$x=PdoNesti::$monPdo->exec($req);
		

		return $id;

	}
	// ajoute une adresse pour un client
	public function ajoutAdresseClient($idClient,$idAdresse){
		$req="insert into AdresseClient VALUES($idClient,$idAdresse)";
		$x=PdoNesti::$monPdo->exec($req);
		

	}

	// récupère les adresses d'un client
	public function getAdresses($idClient){
		
		$req='select ac.idAdresse, a.rue,a.cp,a.ville FROM AdresseClient as ac
		join adresse as a
		ON ac.idAdresse = a.id
		where ac.idClient= :i';
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idClient);
		$res->execute();
		$lesAdresses = $res->fetchAll();

		
		return $lesAdresses;
	}
	// supprime l'adresse d'un client
	public function suprrimerAdresse($idClient,$idAdresse){
		$req= "delete from AdresseClient where idClient = :ic and idAdresse = :ia";
		$res = PdoNesti::$monPdo->prepare($req);
		$res->bindValue(":ic",$idClient);
		$res->bindValue(":ia",$idAdresse);
		$res->execute();
		

	}
	//récupère les recettes en fonction des catégories
	public function getRecettesParCat($idCat){
		$req="select r.id,r.nomRec,r.description,r.image,c.nom from Recette as r 
		join Categorie as c 
		on r.idCat = c.id
		where idCat = :i";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idCat);
		$res->execute();
		$lesRecettes = $res->fetchAll();

		return $lesRecettes;
	}

	// récupère le contenu des recettes en fonction de l'id de la recette
	public function getContenuRecette($idRec){

		$req= "select r.nomRec,r.description,r.image,i.nom,c.quantite 
		from Recette as r
		join ContenuRecette as c
		on c.idRec = r.id
		join Ingredients as i 
		on c.idIng = i.idIng where c.idRec = $idRec";
		$res = PdoNesti::$monPdo->query($req);
		$lesRecettes = $res->fetchAll();
		return $lesRecettes;


	}

//fonction pour creer une commande

	public function insertCommande($idClient,$idAdresse){
		
		$getMax = "select max(id) from Commandes";
		$res = PdoNesti::$monPdo->query($getMax);
		$id = $res->fetchColumn();
		$id++;
		$req="insert into Commandes(id,idClient,idAdresse) VALUES($id,$idClient,$idAdresse)";
		$x=PdoNesti::$monPdo->exec($req);
		return $id;

	}
	// crée le contenu d'une commande
	public function creerCommande($idComm,$idIng,$idCond,$qute){
		

		//booleen controle si l'insertion a réussi
		$succes = false;
		$req ="insert into contenuCommande values($idComm,$idIng,$idCond,$qute)";
		$res = PdoNesti::$monPdo->query($req);
		
		if($res){
			$succes = true;

		}
		
		return $succes;
	}
	//fonction pour afficher les commandes d'un client
	public function getCommandesClient($idClient){
		$req = "select c.id,a.rue,a.ville,a.cp from Commandes as c join adresse as a on a.id = c.idAdresse where idClient = :i ";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idClient);
		$res->execute();
		$lesCommandes = $res->fetchAll(); //recupere uniquement la colonne de l'id

		return $lesCommandes;
	}



		//recupère les cours en fonction des catégories

	public function getCoursDeCategories($idCat){
		$req = "select * from Cours as c  
		join Recette as r on c.idrec = r.id 
		join Cuisinier as cuis
		on c.idCuisinier = cuis.idCuisinier
		join Utilisateur as u 
		on cuis.idCuisinier = u.id
		join Lieux as l 
		on c.idLieux = l.idLieux
		JOIN PlageHoraire as p 
		on p.id = c.idPlageHoraire
		join Categorie as cat 
		on r.idCat = cat.id
		join adresse as a 
		on l.idAdresse = a.id
		join Specialite as s
		on s.id = idSpecialite
		where r.idCat = :i ";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idCat);
		$res->execute();
		$lesCours=$res->fetchAll();
		return $lesCours;


	}


	// inscription du client à un cours

	public function inscriptionCour($idClient,$idCours){
		$succes = false;
		$requete = "select * from inscriptionCour where idUtil = :i and idCour = :ic";
		$res = PdoNesti::$monPdo->prepare($requete); 
		$res->bindValue(":i",$idClient);
		$res->bindValue(":ic",$idCours);
		$res->execute();
		$lesLignes = $res->rowCount();
		var_dump($lesLignes);
		
		if ($lesLignes < 1){


		$req = 'insert into inscriptionCour values(:ic,:ico)';
		$res = PdoNesti::$monPdo->prepare($req); 
		$res-> bindValue(":ic",$idClient);
		$res-> bindValue(":ico",$idCours);
		$res->execute();

		
		$succes = true;
		
		
	}
	else {
		$succes = false;
	}
		return $succes;
	}



	
	// récupère les cours du client 
	public function afficheCoursParClient($idClient){
		$req = "select * from inscriptionCour as ic
		join Cours as co 
		on ic.idCour = co.idCour
		join Cuisinier as c 
		on co.idCuisinier = c.idCuisinier
		join Recette as r 
		on r.id = co.idRec
		JOIN Lieux as l 
		on l.idLieux = co.idLieux
		JOIN PlageHoraire as p 
		on p.id = co.idPlageHoraire
		where ic.idUtil = :i";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idClient);
		$res->execute();
		$lesCoursParClient=$res->fetchAll();
		return $lesCoursParClient;


	}

	// supprimer le cour d'un client

	public function deleteCourClient($idClient,$idCour){

		$req = "delete FROM Customers WHERE idCour = :ic and idUtil = :i";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idClient);
		$res->bindValue(":ic",$idCour);
		$res->execute();

	}

	//récupère les ingredients d'une commande en fonction de l'id de la commande

	public function getIngCommande($idComm){
		$req = "select * from contenuCommande as c join Ingredients as i on c.idIng = i.idIng join Conditionnement as cond on c.idCond = cond.id join Tarif as t on t.idIng = i.idIng where c.idComm = :i";
		$res = PdoNesti::$monPdo->prepare($req); 
		$res->bindValue(":i",$idComm);
		$res->execute();
		$lesCommandes=$res->fetchAll();

		return $lesCommandes;


	}

























}




























