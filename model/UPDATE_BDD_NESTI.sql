-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 09, 2019 at 08:36 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `nesti`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(11) NOT NULL,
  `rue` varchar(250) NOT NULL,
  `cp` int(11) NOT NULL,
  `ville` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adresse`
--

INSERT INTO `adresse` (`id`, `rue`, `cp`, `ville`) VALUES
(1, '16 avenue du poilu', 34110, 'MIREVAL'),
(2, '12 Avenue de la Mission', 34110, 'vic la gardiole'),
(3, '1 r Mas de Lapierre', 34110, 'frontignan'),
(4, '3 Rue Barnier', 34110, 'vic la gardiole'),
(5, '3 Rue Barnier', 34110, 'vic la gardiole'),
(6, '3 Rue Barnier', 34110, 'vic la gardiole'),
(7, '3 Rue Barnier', 34110, 'vic la gardiole'),
(8, '3 Rue Barnier', 34110, 'vic la gardiole'),
(9, '4 Rue Barnier', 34110, 'frontignan'),
(10, 'Darse 2', 34200, 'sete'),
(11, '10 rue Mercier', 34200, 'SETE'),
(12, '13 rue Mercier', 34200, 'SETE'),
(13, '17 rue Mercier', 34200, 'SETE'),
(14, '18 avenue du poilu', 34110, 'Mireval'),
(15, '1 rue des Lilas', 34200, 'SETE');

-- --------------------------------------------------------

--
-- Table structure for table `AdresseClient`
--

CREATE TABLE `AdresseClient` (
  `idClient` int(11) NOT NULL,
  `idAdresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Categorie`
--

CREATE TABLE `Categorie` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `images` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Categorie`
--

INSERT INTO `Categorie` (`id`, `nom`, `images`) VALUES
(1, 'produits frais', 'images/categories/frais.png'),
(2, 'Surgelé', 'images/categories/Surgelé.png'),
(3, 'Epicerie sucré', 'images/categories/sucre.png'),
(4, 'Epicerie salé', 'images/categories/Epiceries.png'),
(6, 'Boissons', 'images/categories/Boissons.png'),
(7, 'Bio', 'images/categories/bio.png'),
(8, 'Fruits&Légumes', 'images/categories/f&l.png');

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Commandes`
--

CREATE TABLE `Commandes` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `etat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Commandes`
--

INSERT INTO `Commandes` (`id`, `idClient`, `etat`) VALUES
(1, 1, NULL),
(2, 1, NULL),
(3, 1, NULL),
(4, 1, NULL),
(5, 1, NULL),
(6, 1, NULL),
(7, 1, NULL),
(8, 1, NULL),
(9, 1, NULL),
(10, 1, NULL),
(11, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Conditionnement`
--

CREATE TABLE `Conditionnement` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Conditionnement`
--

INSERT INTO `Conditionnement` (`id`, `nom`) VALUES
(3, '1 Litre'),
(5, '1 pièce'),
(6, '1kg'),
(4, '500g'),
(2, 'pack de 10'),
(1, 'pack de 6');

-- --------------------------------------------------------

--
-- Table structure for table `contenuCommande`
--

CREATE TABLE `contenuCommande` (
  `idComm` int(11) NOT NULL,
  `idIng` int(11) NOT NULL,
  `idCond` int(11) NOT NULL,
  `quantite` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contenuCommande`
--

INSERT INTO `contenuCommande` (`idComm`, `idIng`, `idCond`, `quantite`) VALUES
(2, 1, 2, 1),
(2, 9, 5, 1),
(3, 1, 2, 1),
(3, 5, 6, 1),
(3, 9, 5, 1),
(4, 1, 2, 1),
(4, 2, 6, 1),
(4, 9, 5, 74),
(5, 1, 2, 1),
(5, 2, 6, 1),
(5, 9, 5, 74),
(6, 1, 2, 1),
(6, 2, 6, 1),
(6, 9, 5, 74),
(7, 1, 2, 1),
(7, 2, 6, 1),
(7, 9, 5, 74),
(8, 1, 2, 1),
(8, 2, 6, 1),
(8, 9, 5, 74),
(9, 1, 2, 1),
(9, 2, 6, 1),
(9, 9, 5, 74),
(10, 1, 2, 1),
(10, 2, 6, 1),
(10, 9, 5, 74),
(11, 1, 2, 1),
(11, 2, 6, 1),
(11, 9, 5, 74);

-- --------------------------------------------------------

--
-- Table structure for table `ContenuRecette`
--

CREATE TABLE `ContenuRecette` (
  `idRec` int(11) NOT NULL,
  `idIng` int(11) NOT NULL,
  `quantite` varchar(150) NOT NULL,
  `idCond` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ContenuRecette`
--

INSERT INTO `ContenuRecette` (`idRec`, `idIng`, `quantite`, `idCond`) VALUES
(13, 1, '1', 5),
(1, 2, '1', 6),
(5, 2, '1', 4),
(2, 7, '2', 4),
(2, 8, '2', 5);

-- --------------------------------------------------------

--
-- Table structure for table `Cours`
--

CREATE TABLE `Cours` (
  `idCour` int(11) NOT NULL,
  `idRec` int(11) NOT NULL,
  `idCuisinier` int(11) NOT NULL,
  `idLieux` int(11) NOT NULL,
  `idPlageHoraire` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Cours`
--

INSERT INTO `Cours` (`idCour`, `idRec`, `idCuisinier`, `idLieux`, `idPlageHoraire`, `date`) VALUES
(1, 2, 11, 3, 2, '2012-12-12'),
(2, 7, 11, 4, 2, '2019-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `Cuisinier`
--

CREATE TABLE `Cuisinier` (
  `idCuisinier` int(11) NOT NULL,
  `idSpecialite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Cuisinier`
--

INSERT INTO `Cuisinier` (`idCuisinier`, `idSpecialite`) VALUES
(11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Droits`
--

CREATE TABLE `Droits` (
  `id` int(11) NOT NULL,
  `nom` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Droits`
--

INSERT INTO `Droits` (`id`, `nom`) VALUES
(2, 'ADMIN'),
(3, 'CLIENT'),
(1, 'CUISINIER'),
(4, 'MANAGER');

-- --------------------------------------------------------

--
-- Table structure for table `EtatCommande`
--

CREATE TABLE `EtatCommande` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Ingredients`
--

CREATE TABLE `Ingredients` (
  `idIng` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `images` varchar(150) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Ingredients`
--

INSERT INTO `Ingredients` (`idIng`, `nom`, `idCategorie`, `images`, `description`) VALUES
(1, 'steack haché', 2, 'images/ingredients/steack.jpg', 'bjr'),
(2, 'pommes de terres', 8, 'images/ingredients/pdt.jpg', 'frais'),
(5, 'farine complète', 7, 'images/ingredients/farinebio.jpg', 'Farine biologique origine france'),
(6, 'tomates', 8, 'images/ingredients/tomate.jpg', ''),
(7, 'bananes', 8, 'images/ingredients/banane.jpg', ''),
(8, 'chocolat noir', 3, 'images/ingredients/chocolat.jpg', ''),
(9, 'gigot agneau', 1, 'images/ingredients/gigot.jpg', ''),
(10, 'poulet entier', 1, 'images/ingredients/poulet.jpg', 'Poulet origine France'),
(11, 'pommes', 8, 'images/ingredients/pommes.jpg', ''),
(12, 'Lait', 6, 'images/ingredients/lait.jpg', ''),
(13, 'pavé Saumon', 1, 'images/ingredients/saumon.jpg', ''),
(17, 'Fraises', 8, 'Fraises provenance Espagne', 'images/ingredients/fraises.jpg'),
(18, 'Pates', 4, 'pates ', 'images/'),
(20, 'Salade', 8, 'Salade origine France', 'images/ingredients/salade.jpg'),
(21, 'Entrecôte', 1, 'dlkfmoierj', 'images/ingredients/entrecote.jpg'),
(22, 'Levure chimique', 3, 'iojlkjh', 'ioju'),
(26, 'Canard ', 1, 'canard gersois ', 'images/ingredients/canard.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `inscriptionCour`
--

CREATE TABLE `inscriptionCour` (
  `idUtil` int(11) NOT NULL,
  `idCour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Lieux`
--

CREATE TABLE `Lieux` (
  `idLieux` int(11) NOT NULL,
  `nomLieu` varchar(100) NOT NULL,
  `idAdresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Lieux`
--

INSERT INTO `Lieux` (`idLieux`, `nomLieu`, `idAdresse`) VALUES
(1, 'MJC', 6),
(2, 'Salle Leo Mallet', 14),
(3, 'Maison communale', 13),
(4, 'Maison culturelle', 1),
(5, 'Association cuisine', 14);

-- --------------------------------------------------------

--
-- Table structure for table `PlageHoraire`
--

CREATE TABLE `PlageHoraire` (
  `id` int(11) NOT NULL,
  `debut` time DEFAULT NULL,
  `fin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PlageHoraire`
--

INSERT INTO `PlageHoraire` (`id`, `debut`, `fin`) VALUES
(1, '00:00:00', '00:00:00'),
(2, '08:00:00', '12:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Recette`
--

CREATE TABLE `Recette` (
  `id` int(11) NOT NULL,
  `nomRec` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `idCat` int(11) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Recette`
--

INSERT INTO `Recette` (`id`, `nomRec`, `description`, `idCat`, `image`) VALUES
(1, 'Pizza 3 Fromages', 'Etaler la pâte à pizza dans un plat à tarte, la piquer avec une fourchette, étaler le coulis de tomate sur la pâte.\r\nDécouper des rondelles de fromage de chèvre et de mozzarella, découper aussi de fines tranches de roquefort.\r\nPlacer le fromage en alternance (une tranche de fromage de chèvre, une de mozzarella, une de roquefort).\r\nCouvrir de gruyère râpé.\r\nSaler et poivrer selon les goûts.\r\nMettre au four à Thermostat 7 (210°C), pendant 1/2 heure et plus si nécessaire.\r\nNote de l\'auteur\r\n\r\nCela fait une bonne pizza avec une bonne épaisseur ; se mange facilement pour ceux qui aiment les fromages. On peut la faire avec n\'importe quel fromage. En général, elle a du succès. ', 1, 'images/recettes/pizza3from.jpg'),
(2, 'Tiramisu', 'La préparation de la recette\r\n\r\n    Préparer un café très fort, y ajouter l’alcool et laisser refroidir.\r\n\r\n    Mettre le sucre dans le bol et mixer 10 sec/vitesse 9.\r\n\r\n    Ajouter les jaunes d’œufs, le mascarpone et le sucre vanillé et mixer 15 sec/vitesse 6.\r\n\r\n    Verser la crème au mascarpone dans un saladier et nettoyer le bol. Insérer le fouet dans le bol, ajouter les 2 blancs d’œufs et le sel et régler 3 min/vitesse 4.\r\n\r\n    Incorporer délicatement les blancs en neige à la crème au mascarpone. Imbiber, sans les laisser tremper dans le café, 10 ou 12 biscuits à la cuiller et en tapisser le fond d’un plat rectangulaire transparent (mettre la face plate du biscuit, la plus imbibée, au-dessous).\r\n\r\nRecouvrir les biscuits de la moitié de la crème au mascarpone. Ajouter une couche de biscuits imbibés et finir avec la crème au mascarpone.\r\n\r\n    Recouvrir le plat d’une feuille de film alimentaire et mettre au réfrigérateur pendant au moins 6 heures. L’idéal est de faire ce gâteau la veille pour que le café imprègne bien les biscuits. Au moment de servir, retirer le film et saupoudrer d’une bonne couche de cacao amer. \r\n\r\nAstuce\r\n\r\nConseil Thermomix : Le mascarpone est un fromage blanc très crémeux que l’on trouve au rayon fromages emballés des grandes surfaces.', 3, 'images/recettes/tiramisu.jpg'),
(5, 'Moussaka', 'Dans un plat profond allant au four, \ndisposez une couche de viande,\n une couche de tomates, une couche \nd aubergines, une couche de pommes\n de terre. Préparez une sauce béchamel \navec le beurre, la farine et le lait. \nAssaisonnez, ajoutez les jaunes d oeufs hors\n du feu et le fromage râpé.', 2, 'images/recettes/moussaka.jpg'),
(6, 'Pâtes à la Carbonara', 'dflmskjfmishqcjkhvmsdis:', 4, 'null'),
(7, 'Hachis parmentier de Canard', 'ezeiofjeoimfhsifhumfhl', 1, 'null'),
(9, 'Tarte à la fraise', 'tarte', 3, 'null'),
(10, 'Tajine', 'dlksfojidoimhsimc', 1, 'null'),
(11, 'Couscous', 'dmsflksùpofuo', 1, 'null'),
(12, 'Canelonni', 'dkziojsi', 1, 'null'),
(13, 'Lasagnes', 'Lasagnes', 1, 'null');

-- --------------------------------------------------------

--
-- Table structure for table `Specialite`
--

CREATE TABLE `Specialite` (
  `id` int(11) NOT NULL,
  `specialite` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Specialite`
--

INSERT INTO `Specialite` (`id`, `specialite`) VALUES
(1, 'Patisserie'),
(2, 'Cuisine Italienne'),
(3, 'Cuisine algérienne'),
(4, 'Cuisine Asiatique');

-- --------------------------------------------------------

--
-- Table structure for table `Tarif`
--

CREATE TABLE `Tarif` (
  `idIng` int(11) NOT NULL,
  `idCond` int(11) NOT NULL,
  `tarif` float NOT NULL,
  `devise` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Tarif`
--

INSERT INTO `Tarif` (`idIng`, `idCond`, `tarif`, `devise`) VALUES
(1, 1, 3.7, '€'),
(1, 2, 5.85, '€'),
(6, 4, 0.8, '€'),
(7, 4, 0.85, '€'),
(8, 5, 2.5, '€'),
(9, 5, 29.45, '€'),
(2, 6, 1.25, '€'),
(5, 6, 0.98, '€'),
(7, 6, 1.44, '€'),
(11, 6, 3.56, '€');

-- --------------------------------------------------------

--
-- Table structure for table `Theme`
--

CREATE TABLE `Theme` (
  `id` int(11) NOT NULL,
  `descript` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Theme`
--

INSERT INTO `Theme` (`id`, `descript`) VALUES
(2, 'dessert'),
(5, 'Pâtes'),
(4, 'Poissons'),
(1, 'Salé rapide'),
(6, 'Tartes'),
(3, 'Viandes');

-- --------------------------------------------------------

--
-- Table structure for table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `prenom` varchar(60) NOT NULL,
  `ddn` date DEFAULT NULL,
  `rue` varchar(80) DEFAULT NULL,
  `mail` varchar(80) NOT NULL,
  `idDroits` int(11) DEFAULT NULL,
  `login` varchar(45) NOT NULL,
  `mdp` varchar(250) NOT NULL,
  `cp` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Utilisateur`
--

INSERT INTO `Utilisateur` (`id`, `nom`, `prenom`, `ddn`, `rue`, `mail`, `idDroits`, `login`, `mdp`, `cp`) VALUES
(1, 'vidal', 'camille', '1992-08-15', '16 avenue du poilu', 'camillevidal58@aol.fr', 2, 'camillevidal58', '21232f297a57a5a743894a0e4a801fc3', NULL),
(2, 'Dupond', 'David', '2019-03-12', '13 avenue des Lilas', 'davidDupond@gmail.com', 3, 'davidupond', 'david', NULL),
(3, 'rigal', 'maeva', '2018-07-09', '16 avenue du poilu', 'mae.maissan@gmail.com', 3, 'camillevidal58', 'mae', NULL),
(4, 'VTdal', 'MicheFFl', '1957-12-05', '12 avenue des lilaSS', 'michel@aol.frD', NULL, 'mimi15\"', '9dd4e461268c8034f5c8564e155c67a6', NULL),
(5, 'rigal', 'maeva', '1992-07-02', '16 avenue du poilu', 'maemae@aol.fr', NULL, 'mae34', 'xxxxx', NULL),
(7, '0', '0', NULL, NULL, '0', NULL, '0', '0', NULL),
(8, '0', '0', NULL, NULL, '0', NULL, '0', '0', NULL),
(9, 'titi', 'toto', NULL, NULL, 'toto@aol.fr', NULL, 'toto', 'e34a8899ef6468b74f8a1048419ccc8b', NULL),
(10, 'vidal', 'dom', NULL, NULL, 'vidal.dom@aol.fr', NULL, 'dvidal', '900150983cd24fb0d6963f7d28e17f72', NULL),
(11, 'Delmau', 'Maily', '2019-06-05', NULL, '', 1, 'mailyDelmau', 'x', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `AdresseClient`
--
ALTER TABLE `AdresseClient`
  ADD PRIMARY KEY (`idClient`,`idAdresse`),
  ADD KEY `FK_idAdresse` (`idAdresse`);

--
-- Indexes for table `Categorie`
--
ALTER TABLE `Categorie`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorie_UNIQUE` (`nom`);

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Client_Utilisateur1_idx` (`id`);

--
-- Indexes for table `Commandes`
--
ALTER TABLE `Commandes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_Commandes_EtatCommande1_idx` (`etat`),
  ADD KEY `fk_Commandes_Client1_idx` (`idClient`) USING BTREE;

--
-- Indexes for table `Conditionnement`
--
ALTER TABLE `Conditionnement`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `conditionnement_UNIQUE` (`nom`);

--
-- Indexes for table `contenuCommande`
--
ALTER TABLE `contenuCommande`
  ADD PRIMARY KEY (`idComm`,`idIng`),
  ADD KEY `contenucommande_ibfk_2` (`idIng`);

--
-- Indexes for table `ContenuRecette`
--
ALTER TABLE `ContenuRecette`
  ADD PRIMARY KEY (`idIng`,`idRec`),
  ADD KEY `fk_Ingredients_has_Recette_Recette1_idx` (`idRec`),
  ADD KEY `fk_Ingredients_has_Recette_Ingredients1_idx` (`idIng`),
  ADD KEY `hasCond` (`idCond`);

--
-- Indexes for table `Cours`
--
ALTER TABLE `Cours`
  ADD PRIMARY KEY (`idCour`),
  ADD KEY `date` (`date`,`idPlageHoraire`,`idCuisinier`) USING BTREE,
  ADD KEY `hasCuisinier` (`idCuisinier`),
  ADD KEY `hasLieux` (`idLieux`),
  ADD KEY `hasPlageHoraire` (`idPlageHoraire`),
  ADD KEY `hasRecette` (`idRec`);

--
-- Indexes for table `Cuisinier`
--
ALTER TABLE `Cuisinier`
  ADD PRIMARY KEY (`idCuisinier`),
  ADD KEY `fk_Cuisinier_Utilisateur1_idx` (`idCuisinier`),
  ADD KEY `fk_Cuisinier_Specialite1_idx` (`idSpecialite`);

--
-- Indexes for table `Droits`
--
ALTER TABLE `Droits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nomDroits_UNIQUE` (`nom`);

--
-- Indexes for table `EtatCommande`
--
ALTER TABLE `EtatCommande`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Ingredients`
--
ALTER TABLE `Ingredients`
  ADD PRIMARY KEY (`idIng`),
  ADD KEY `fk_Ingredients_Categorie1_idx` (`idCategorie`);

--
-- Indexes for table `inscriptionCour`
--
ALTER TABLE `inscriptionCour`
  ADD PRIMARY KEY (`idUtil`,`idCour`),
  ADD KEY `idUtil` (`idUtil`),
  ADD KEY `hasCour` (`idCour`);

--
-- Indexes for table `Lieux`
--
ALTER TABLE `Lieux`
  ADD PRIMARY KEY (`idLieux`),
  ADD KEY `hasAdresse` (`idAdresse`);

--
-- Indexes for table `PlageHoraire`
--
ALTER TABLE `PlageHoraire`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Recette`
--
ALTER TABLE `Recette`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hasCategorie` (`idCat`);

--
-- Indexes for table `Specialite`
--
ALTER TABLE `Specialite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tarif`
--
ALTER TABLE `Tarif`
  ADD PRIMARY KEY (`idCond`,`idIng`),
  ADD KEY `fk_Conditionnement_has_Ingredients_Ingredients1_idx` (`idIng`),
  ADD KEY `fk_Conditionnement_has_Ingredients_Conditionnement1_idx` (`idCond`);

--
-- Indexes for table `Theme`
--
ALTER TABLE `Theme`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `descript_UNIQUE` (`descript`);

--
-- Indexes for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Utilisateur_Droits1_idx` (`idDroits`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Categorie`
--
ALTER TABLE `Categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Conditionnement`
--
ALTER TABLE `Conditionnement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Cours`
--
ALTER TABLE `Cours`
  MODIFY `idCour` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `Droits`
--
ALTER TABLE `Droits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Ingredients`
--
ALTER TABLE `Ingredients`
  MODIFY `idIng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `Lieux`
--
ALTER TABLE `Lieux`
  MODIFY `idLieux` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `PlageHoraire`
--
ALTER TABLE `PlageHoraire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Recette`
--
ALTER TABLE `Recette`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `Specialite`
--
ALTER TABLE `Specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Theme`
--
ALTER TABLE `Theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AdresseClient`
--
ALTER TABLE `AdresseClient`
  ADD CONSTRAINT `FK_idAdresse` FOREIGN KEY (`idAdresse`) REFERENCES `adresse` (`id`),
  ADD CONSTRAINT `FK_idClient` FOREIGN KEY (`idClient`) REFERENCES `Utilisateur` (`id`);

--
-- Constraints for table `Client`
--
ALTER TABLE `Client`
  ADD CONSTRAINT `fk_Client_Utilisateur1` FOREIGN KEY (`id`) REFERENCES `Utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Commandes`
--
ALTER TABLE `Commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `Utilisateur` (`id`),
  ADD CONSTRAINT `fk_Commandes_EtatCommande1` FOREIGN KEY (`etat`) REFERENCES `EtatCommande` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contenuCommande`
--
ALTER TABLE `contenuCommande`
  ADD CONSTRAINT `contenucommande_ibfk_1` FOREIGN KEY (`idComm`) REFERENCES `Commandes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contenucommande_ibfk_2` FOREIGN KEY (`idIng`) REFERENCES `Ingredients` (`idIng`) ON DELETE CASCADE;

--
-- Constraints for table `ContenuRecette`
--
ALTER TABLE `ContenuRecette`
  ADD CONSTRAINT `fk_Ingredients_has_Recette_Ingredients1` FOREIGN KEY (`idIng`) REFERENCES `Ingredients` (`idIng`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Ingredients_has_Recette_Recette1` FOREIGN KEY (`idRec`) REFERENCES `Recette` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hasCond` FOREIGN KEY (`idCond`) REFERENCES `Conditionnement` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Cours`
--
ALTER TABLE `Cours`
  ADD CONSTRAINT `hasCuisinier` FOREIGN KEY (`idCuisinier`) REFERENCES `Cuisinier` (`idCuisinier`) ON DELETE CASCADE,
  ADD CONSTRAINT `hasLieux` FOREIGN KEY (`idLieux`) REFERENCES `Lieux` (`idLieux`) ON DELETE CASCADE,
  ADD CONSTRAINT `hasPlageHoraire` FOREIGN KEY (`idPlageHoraire`) REFERENCES `PlageHoraire` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hasRecette` FOREIGN KEY (`idRec`) REFERENCES `Recette` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Cuisinier`
--
ALTER TABLE `Cuisinier`
  ADD CONSTRAINT `fk_Cuisinier_Specialite1` FOREIGN KEY (`idSpecialite`) REFERENCES `Specialite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cuisinier_Utilisateur1` FOREIGN KEY (`idCuisinier`) REFERENCES `Utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Ingredients`
--
ALTER TABLE `Ingredients`
  ADD CONSTRAINT `fk_Ingredients_Categorie1` FOREIGN KEY (`idCategorie`) REFERENCES `Categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inscriptionCour`
--
ALTER TABLE `inscriptionCour`
  ADD CONSTRAINT `hasCour` FOREIGN KEY (`idCour`) REFERENCES `Cours` (`idCour`) ON DELETE CASCADE,
  ADD CONSTRAINT `hasUtilisateur` FOREIGN KEY (`idUtil`) REFERENCES `Utilisateur` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Lieux`
--
ALTER TABLE `Lieux`
  ADD CONSTRAINT `hasAdresse` FOREIGN KEY (`idAdresse`) REFERENCES `adresse` (`id`);

--
-- Constraints for table `Recette`
--
ALTER TABLE `Recette`
  ADD CONSTRAINT `hasCategorie` FOREIGN KEY (`idCat`) REFERENCES `Categorie` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Tarif`
--
ALTER TABLE `Tarif`
  ADD CONSTRAINT `fk_Conditionnement_has_Ingredients_Conditionnement1` FOREIGN KEY (`idCond`) REFERENCES `Conditionnement` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Conditionnement_has_Ingredients_Ingredients1` FOREIGN KEY (`idIng`) REFERENCES `Ingredients` (`idIng`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD CONSTRAINT `fk_Utilisateur_Droits1` FOREIGN KEY (`idDroits`) REFERENCES `Droits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
