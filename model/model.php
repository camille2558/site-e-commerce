<?php


function initPanier($value){
	setCookie("panier",$value,time()+365*24*3600); //cookie pour une durée de an et force le passage par HTTPS pour plus de sécurité
}

//fonction qui supprime le panier
function deletePanier(){
	setcookie("panier","",time()-3600*24);
	unset($_COOKIE['panier']);
	
}

//fonction qui ajoute un ingredient au panier : elle teste si l'id de l'ing est dans la var SESSION 
function ajouterPanier($idIng,$idCond){
	

	if (isset($_COOKIE['panier'])) {

		$lePanierSerialise =$_COOKIE['panier'];
		$arrayPanier = json_decode($lePanierSerialise, true); //true permet de passer de l'objet à l'array
	}else{
		echo 'creation de panier';
		$arrayPanier=[];
	}

// ajoute l'ingredient
//ajout des paramétres dans l'objet
	$arrayPanier[]=[
		'idIng' => $idIng,
		'idCond' => $idCond,
		'qute' => 1
		
	];
	$serialize= json_encode($arrayPanier);
	initPanier($serialize);
	return true;
}
function supprimerArticle($idIng,$idCond){ 
	$lePanier= json_decode($_COOKIE['panier'],true);
	$succes= false;

	foreach ($lePanier as $key => $unPanier) { 

		if ($unPanier['idIng'] == $idIng && $unPanier['idCond'] == $idCond) {
			

			$succes = true;
			unset($lePanier[$key]); // on supprime l'objet entier
		}
	}

	$serialize= json_encode($lePanier); //il faut réencoder le panier et le réinitialiser
	initPanier($serialize);
	return 	true;
}
function ajoutQuantiteIng($id){
	$lePanier= json_decode($_COOKIE['panier'],true);
	foreach ($lePanier as $key => $unPanier) { //key est l'indice et unPanier un tableau


	if ($unPanier['idIng'] == $id) {

		$qute = $unPanier['qute'];
		$qute++;
		$lePanier[$key]['qute']=$qute;

	}

}
$serialize= json_encode($lePanier); //il faut réencoder le panier et le réinitialiser
initPanier($serialize);
return $serialize;

}
function enleverQuantiteIng($id,$idCond){

	$lePanier= json_decode($_COOKIE['panier'],true);
	foreach ($lePanier as $key => $unPanier) { //key est l'indice et unPanier un tableau
	if ($unPanier['idIng'] == $id) {

		$qute = $unPanier['qute'];
		$qute--;
		$lePanier[$key]['qute']=$qute;
		

	}

	if($qute<=0){

		unset($lePanier[$key]);

	}
}
$serialize= json_encode($lePanier); //il faut réencoder le panier et le réinitialiser
initPanier($serialize);

return $lePanier;

}


function deconnectUser(){

	unset($_SESSION['id']);
	session_destroy();

}


?>































