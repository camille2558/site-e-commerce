<div class='ajoutAdress container'>
	<h4 id='titre-adr'>Ajouter une nouvelle adresse</h4>
	<form action="index.php?uc=login&action=enregistrerAdress" class="form" method="POST">
		<div class="form-group">
			<label for="form-address">Rue</label>
			<input type="search" class="form-control" id="form-address" placeholder="Votre adresse" name="rue">
		</div>
		<div class="form-group">
			<label for="form-city">Ville</label>
			<input type="text" class="form-control" id="form-city" placeholder="Votre ville.." name='ville'>
		</div>
		<div class="form-group">
			<label for="form-zip">Code postal</label>
			<input type="text" class="form-control" id="form-zip" placeholder="Cp.." name='cp'>
		</div>
		<button type="submit" class="btn btn-light lien-profil btn-adresse">
			Enregistrer
		</button>
	</form>
</div>

<script src="https://cdn.jsdelivr.net/npm/places.js@1.16.4"></script>
<script>
	(function() {
		var placesAutocomplete = places({
			appId: 'pl0XYUUAXPQO',
			apiKey: '97663c39a991b803b3c7aa91de88449a',
			container: document.querySelector('#form-address'),
			templates: {
				value: function(suggestion) {
					return suggestion.name;
				}
			}
		}).configure({
			type: 'address'
		});
		placesAutocomplete.on('change', function resultSelected(e) {
			document.querySelector('#form-address2').value = e.suggestion.administrative || '';
			document.querySelector('#form-city').value = e.suggestion.city || '';
			document.querySelector('#form-zip').value = e.suggestion.postcode || '';
		});
	})();
</script>

