<div id="login">
	<div class="container">
		
		<div class="col-6 col-md-4 login-form">
			<h3 class="titre-login">Login</h3>
			<form method="POST" action="index.php?uc=login&action=inscript">
				<div class="form-group">
					<label for="email">Login:</label>
					<input type="text" class="form-control" id="login-1" placeholder="Entez votre nom utilisateur" name="login">
				</div>
				<div class="form-group">
					<label for="pwd">Mot de passe:</label>
					<input type="password" class="form-control" id="pwd" placeholder="Entez votre mot de passe" name="mdp">
				</div>
				<div class="form-group form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" name="remember"> Se souvenir de moi
					</label>
				</div>
				<button type="submit" class="btn btn-secondary btn-login">Envoyer</button>

			</form>


		</div>
		
		<?php

			if ($ajoutClient != true) {
				echo "

				<div class='col-6 col-md-4 inscrivez-vous'>

				<h4>Pas encore client chez nous ?</h4>
				<div id='connect'><a class='lien-connect' href='index.php?uc=login&action=nvxclient'>Inscrivez-vous gratuitement</a></div>
				</div>";
			}
		

		?>
	</div>

</div>